# mt-cp
Windows tool to copy files or streams from/to a tape device under Windows.  
Please use in conjunction with [mt-win](https://gitlab.com/notepass/mt-win) to seek, write filemarks and so on.  
This tool is written in C# (.NET 4.0 and above) and only intended for use on Windows based systems with support for .NET 4.0 or newer.  
As the tape APIs haven't changed since Win XP according to the Microsoft docs, this should mean that this tool should be compatible
with Windows XP and newer (32 + 64 bit), although it has only been tested on Windows 10.

# Project state
This tool is still WIP and not ready for use. I will try to finish it somewhere in 2022. 