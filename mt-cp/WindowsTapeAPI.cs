﻿using System;
using System.Runtime.InteropServices;

namespace mt_cp
{
    // TODO: Make this a lib between mt-win and mt-cp
    public class WindowsTapeAPI
    {
        /// ============================== HANDLES ============================== ///

        public const uint GENERIC_READ = 0x80000000;

        public const uint GENERIC_WRITE = 0x40000000;

        public const uint CREATE_NEW = 1;
        public const uint CREATE_ALWAYS = 2;
        public const uint OPEN_EXISTING = 3;
        public const uint OPEN_ALWAYS = 4;
        public const uint TRUNCATE_EXISTING = 5;

        [StructLayout(LayoutKind.Sequential)]
        public class SecurityAttributes
        {
            public uint size;
            public uint securityDescriptor;
            public uint inheritHandle;
        }

        [DllImport("kernel32.dll")]
        public static extern uint CreateFile(
            string lpFileName,
            uint dwDesiredAccess,
            uint dwShareMode,
            SecurityAttributes lpSecurityAttributes,
            uint dwCreationDisposition,
            uint dwFlagsAndAttributes,
            uint hTemplateFile
        );

        [DllImport("kernel32.dll")]
        public static extern uint CloseHandle(uint handle);
        
        

        /// ============================== TAPE INTERACTION ============================== ///

        [StructLayout(LayoutKind.Sequential)]
        public class TAPE_GET_MEDIA_PARAMETERS
        {
            public ulong Capacity;
            public ulong Remaining;
            public uint BlockSize;
            public uint PartitionCount;
            public bool WriteProtected;
        }
        
        public const uint GET_TAPE_MEDIA_INFORMATION = 0;
        public const uint GET_TAPE_DRIVE_INFORMATION = 1;
        
        [DllImport("kernel32.dll")]
        public static extern uint GetTapeParameters(
            uint hDevice,
            uint dwOperation,
            out uint lpdwSize,
            TAPE_GET_MEDIA_PARAMETERS lpTapeInformation
        );

        public const uint ERROR_NO_MEDIA_IN_DRIVE = 1112;
        public const uint ERROR_WRITE_PROTECT = 15;
        public const uint ERROR_FILEMARK_DETECTED = 1101;
        
        [DllImport("kernel32.dll")]
        public static extern uint GetTapeStatus(uint tapeHandle);
        

        /// ============================== I/0 ============================== ///

        [DllImport("kernel32.dll")]
        public static extern bool ReadFile(
            uint hFile,
            byte[] buffer,
            uint nNumberOfBytesToRead,
            out uint lpNumberOfBytesRead,
            object lpOverlapped
        );

        [DllImport("kernel32.dll")]
        public static extern bool WriteFile(
            uint hFile,
            byte[] lpBuffer,
            uint nNumberOfBytesToWrite,
            out uint lpNumberOfBytesWritten,
            object lpOverlapped
        );
    }
}