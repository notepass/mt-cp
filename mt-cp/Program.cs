﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Threading;

namespace mt_cp
{
    internal class Program
    {
        private static bool silent = false;
        private static bool readStdIn = false;
        private static bool showHelp = false;
        private static uint blockSize = 65536;
        private static string[] filesToRead;
        private static readonly string DEFAULT_TAPE_DEVICE = Environment.GetEnvironmentVariable("TAPE") ?? @"\\.\TAPE0";
        private static string tapeDevice = DEFAULT_TAPE_DEVICE;
        private static uint tapeHandle;
        private static bool printThreadRunning = false;
        private static Thread printThread;

        private static long overallBytesToWrite;
        private static ulong overallBytesWritten;
        private static int startTime;
        private const ulong BYTES_TO_MBYTE = 1024 * 1024;
        private const ulong BYTES_TO_GBYTE = BYTES_TO_MBYTE * 1024;
        private const int MS_TO_SEC = 1000;
        

        public static void Main(string[] args)
        {
            try
            {
                args = ScanAndRemoveArgs(args);
                printThreadRunning = !silent;

                if (showHelp)
                {
                    ProcessHelp();
                    return;
                }

                filesToRead = args;

                if (!OpenAndCheckTapeDrive())
                {
                    return;
                }
                
                ProcessWrite();
            }
            finally
            {
                WindowsTapeAPI.CloseHandle(tapeHandle);
                printThreadRunning = false;
                if (printThread != null)
                {
                    printThread.Interrupt();
                }
            }
        }

        public static void ProcessHelp()
        {
            Console.Out.WriteLine("mt-cp [Args ...] [Files ...]");
            Console.Out.WriteLine("Arguments:");
            Console.Out.WriteLine("-f / --tape-file: Tape file to use (Can also be passed as ENVVAR TAPE)");
            Console.Out.WriteLine("-s / --silent: Do not output progress info");
            Console.Out.WriteLine("-h / --help: Show this help");
            Console.Out.WriteLine("- / --stream: Read from stdin / write to stdout, not from files");
            Console.Out.WriteLine();
            Console.Out.WriteLine($"Default tape is {DEFAULT_TAPE_DEVICE}, selected tape device is {tapeDevice}");
            // TODO: Implement
            // Console.Out.WriteLine("-v / --version: Show version");
            //Console.Out.WriteLine("-m / --write-filemark: Write a filemark after each file written and an additional filemark at the end of the tape. Does not work with --stream!");
            //Console.Out.WriteLine("-r / --reverse: Reads from the tape device and writes to the given file(s). Files will be split on filemarks. When --stream is specified, the program will write to stdout until a filemark is encountered");
        }

        public static bool OpenAndCheckTapeDrive()
        {
            tapeHandle = WindowsTapeAPI.CreateFile(tapeDevice,
                WindowsTapeAPI.GENERIC_READ | WindowsTapeAPI.GENERIC_WRITE, 0, null, WindowsTapeAPI.OPEN_EXISTING, 0,
                0);

            if (tapeHandle == UInt32.MaxValue)
            {
                Console.Error.WriteLine("Could not open tape device "+tapeDevice);
                return false;
            }
            
            WindowsTapeAPI.TAPE_GET_MEDIA_PARAMETERS mediaInfo = new WindowsTapeAPI.TAPE_GET_MEDIA_PARAMETERS();
            WindowsTapeAPI.GetTapeParameters(tapeHandle, WindowsTapeAPI.GET_TAPE_MEDIA_INFORMATION, out _, mediaInfo);

            uint tapeState = WindowsTapeAPI.GetTapeStatus(tapeHandle);
            if (tapeState == WindowsTapeAPI.ERROR_NO_MEDIA_IN_DRIVE)
            {
                Console.Error.WriteLine("No media in drive");
                return false;
            }

            if (tapeState == WindowsTapeAPI.ERROR_WRITE_PROTECT)
            {
                Console.Error.WriteLine("Media is write protected");
                return false;
            }

            blockSize = mediaInfo.BlockSize;
            if (blockSize == 0)
            {
                blockSize = 65536;
            }

            return true;
        }

        public static void ProcessWrite()
        {
            printThread = new Thread(new ThreadStart(PrintProgress));
            
            if (readStdIn)
            {
                using (Stream stdin = Console.OpenStandardInput())
                {
                    startTime = Environment.TickCount;
                    printThread.Start();
                    ProcessStream(stdin);
                }
            }
            else
            {
                // Step 1: Check if files exist and get size
                foreach (var file in filesToRead)
                {
                    if (!File.Exists(file))
                    {
                        Console.Error.WriteLine($"Input file {file} does not exist!");
                        return;
                    }

                    overallBytesToWrite += new FileInfo(file).Length;
                }

                startTime = Environment.TickCount;
                printThread.Start();
                // Step 2: Write files
                foreach (var file in filesToRead)
                {
                    using (Stream fileStream = File.OpenRead(file))
                    {
                        ProcessStream(fileStream);
                    }
                }
            }
        }

        private static void ProcessStream(Stream inStream)
        {
            
            int bytesRead;
            uint bytesWritten;
            byte[] buffer = new byte[blockSize];
            while ( (bytesRead = inStream.Read(buffer, 0, buffer.Length)) > 0 )
            {
                // TODO: 0-pad
                // TODO: Check error
                // TODO: Check bytes written
                
                WindowsTapeAPI.WriteFile(tapeHandle, buffer, (uint)buffer.Length, out bytesWritten, null);
                overallBytesWritten += bytesWritten;
            }
        }

        public static string[] ScanAndRemoveArgs(string[] args)
        {
            if (args.Length == 0)
            {
                showHelp = true;
                return args;
            }
            
            List<int> indexesToRemove = new List<int>();

            for (int i = 0; i < args.Length; i++)
            {
                switch (args[i])
                {
                    case "-s":
                    case "--silent":
                        silent = true;
                        indexesToRemove.Add(i);
                        break;
                    
                    case "-":
                    case "--":
                    case "--stream":
                        readStdIn = true;
                        indexesToRemove.Add(i);
                        break;
                    
                    case "-f":
                    case "--tape-file":
                        // TODO: Bound check
                        tapeDevice = args[i + 1];
                        indexesToRemove.Add(i);
                        indexesToRemove.Add(i + 1);
                        break;
                    
                    case "-h":
                    case "--help":
                        showHelp = true;
                        indexesToRemove.Add(i);
                        break;
                }
            }

            if (indexesToRemove.Count == 0)
            {
                return args;
            }

            string[] newArgs = new string[args.Length - indexesToRemove.Count];
            int newIndex = 0;
            for (int i = 0; i < args.Length; i++)
            {
                if (!indexesToRemove.Contains(i))
                {
                    newArgs[newIndex] = args[i];
                    newIndex++;
                }
            }

            return newArgs;
        }

        public static void PrintProgress()
        {
            // TODO: Stop properly
            float mbps = 0;
            float percentage = 0;
            float gbyteWritten;
            float gbyteToWrite = ((float)overallBytesToWrite) / BYTES_TO_GBYTE;
            while (printThreadRunning)
            {
                try
                {
                    gbyteWritten = ((float)overallBytesWritten) / BYTES_TO_GBYTE;
                    mbps = ((((float)overallBytesWritten) / BYTES_TO_MBYTE) / (((float)(Environment.TickCount - startTime)) / MS_TO_SEC ));
                    percentage = ((float)overallBytesToWrite) / overallBytesWritten;
                    
                    if (overallBytesToWrite > 0)
                    {
                        Console.Out.Write($"Wrote {gbyteWritten:0.00} GiB of {gbyteToWrite:0.00} GiB ({percentage:0.00}% @ {mbps:0.00} MByte/sec)\n");
                    }
                    else
                    {
                        Console.Out.Write($"Wrote {gbyteWritten:0.00} GiB @ {mbps:0.00} MByte/sec\n");
                    }
                    
                    Thread.Sleep(1000);
                }
                catch (ThreadInterruptedException)
                {
                    // ignore
                }
            }
        }
    }
}